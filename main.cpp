#include "App.hpp"

int main(int argc, char* argv[])
{
	App app("Potato Map Editor");

	return app.run();
}