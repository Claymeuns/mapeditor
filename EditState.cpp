#include "EditState.hpp"
#include <iostream>

EditState::EditState(App *app)
{
	setApp(app);
}

EditState::~EditState()
{
	dispose();
}

void EditState::init()
{

}

void EditState::handleEvent(sf::Event& event)
{

}

void EditState::update()
{

}

void EditState::render(sf::RenderWindow& window)
{
	window.clear(sf::Color(0,240,240,255));
}

void EditState::dispose()
{

}
