/*
*
*	EditState.hpp: The Edit State, will alow
*	the user to edit a new map or an existing map.
*
*/
#ifndef EDIT_STATE_HPP
#define EDIT_STATE_HPP

#include "State.hpp"
#include "App.hpp"
#include "TextureManager.hpp"
#include <SFML/Graphics.hpp>

///
/// @class EditState EditState.hpp
/// @author Cl�ment Sibille
/// 
class EditState : public State
{
private:
	TextureManager m_textures;
	
public:
	EditState(App* app);
	~EditState();
	virtual void init();
	virtual void handleEvent(sf::Event& event);
	virtual void update();
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();
};

#endif