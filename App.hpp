/*
*
*		App.hpp : App class
*
*
*/

#ifndef APP_HPP
#define APP_HPP

#include <SFML/Graphics.hpp>
#include <string>

#include "StateHandler.hpp"

///
/// @class App App.hpp
/// @author Cl�ment Sibille
/// @brief The main class of the app
/// 
class App
{
	private:
		/// < StateHandler, used to handle the states of the app
		StateHandler m_gsm;


		/// < Title of the app (and of the main window)
		std::string m_title; 

		/// < Main window of the app
		sf::RenderWindow m_window; 

		/// @brief Initialize the main window
		void initWindow(); 

		/// @brief Initialize the state manager
		void initStateManager();
	public:
		App();
		App(std::string title);
		~App();

		/// @brief returns the state manager
		/// @return the state manager
		StateHandler& getStateManager();

		/// @brief returns the app's window
		/// @return the app's window
		sf::RenderWindow& getWindow();



		/// @brief sets the app title
		/// @param title the new title
		void setTitle(std::string title);
		/// @brief returns the title
		/// @return the title
		std::string getTitle();

		/// @brief Starting the job
		/// @return An error code ? Maybe ?
		int run();
};

#endif
