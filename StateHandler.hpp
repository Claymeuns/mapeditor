#ifndef STATE_HANDLER_HPP
#define STATE_HANDLER_HPP

#include <stack>
#include <SFML/Graphics.hpp>

class App;

class State;

///
/// @class StateHandler StateHandler.hpp
/// @author Cl�ment Sibille
/// @brief State Manager
/// 
class StateHandler
{
private:
	/// < Pointer to the app
	App* m_app;

	/// < States stack
	std::stack<State*> m_states;

public:
	StateHandler(App* app);

	void pushState(State* state);
	void popState();
	
	///  @brief Pops the current State and pushes the new State
	void changeState(State* state);
	/// @return the current State (nullptr if m_states is empty)
	State* peekState();

	/// @brief used to initialize things
	void init();

	/// @brief used to manage the events
	/// @param event the SFML event
	void handleEvent(sf::Event& event);

	/// @brief what to do at each frame
	void update();

	/// @brief used to draw things at the screen
	/// @param window the app window
	void render(sf::RenderWindow& window);

	/// @brief used to dispose/delete all things
	void dispose();
};

#endif