#ifndef TEXTURE_MANAGER_HPP
#define TEXTURE_MANAGER_HPP

#include <SFML/Graphics.hpp>
#include <map>
#include <string>

///
/// @class TextureManager TextureManager.hpp
/// @author Cl�ment Sibille
/// @brief To manage textures
/// 
class TextureManager
{
private:
	/// < the textures's map
	std::map<std::string, sf::Texture> m_map;
public:
	TextureManager();

	/// @brief Used to load textures
	/// @param key The key we want to associate to the font
	/// @param textureFile The path to the texture we want to load
	void loadTexture(const std::string& key, const std::string& textureFile);
	
	// @brief returns a texture we loaded before
	/// @return reference to a loaded texture
	sf::Texture& getTexture(const std::string& key);
};

#endif