/*
*
*	State.hpp: States abstract class
*
*
*/


#ifndef STATE_HPP
#define STATE_HPP

#include <SFML/Graphics.hpp>

#include "App.hpp"

///
/// @class State State.hpp
/// @author Cl�ment Sibille
/// @brief Abstract class to make States
/// 
class State
{
private:
	App* m_app;
public:
	virtual ~State(){};

	/// @brief used to initialize things
	virtual void init() =0;

	/// @brief used to manage the events
	/// @param event the SFML event
	virtual void handleEvent(sf::Event& event) =0;

	/// @brief what to do at each frame
	virtual void update()=0;

	/// @brief used to draw things at the screen
	/// @param window the app window
	virtual void render(sf::RenderWindow& window) =0;

	/// @brief used to dispose/delete all things
	virtual void dispose() =0;


	/// @brief setter for m_app
	/// @param app pointer to the app
	void setApp(App* app);
	/// @brief getter for m_app
	/// @return app pointer
	App* getApp();
};

#endif