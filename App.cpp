#include "App.hpp"
#include "MenuState.hpp"

// Default constructor
App::App() :
m_gsm(this)
{
	m_title = "AppTitle";
	initWindow();
	initStateManager();
}

App::App(std::string title) :
m_gsm(this)
{
	m_title = title;
	initWindow();
	initStateManager();
}

// Initialize the window
void App::initWindow()
{
	m_window.create(sf::VideoMode(800,600), m_title);
	m_window.setFramerateLimit(60);
	m_window.setVerticalSyncEnabled(true);
}

// Initialize the StateManager
void App::initStateManager()
{
	m_gsm.init();
}

// Main loop
int App::run()
{
	while(m_window.isOpen())
	{
		sf::Event event;
		while(m_window.pollEvent(event))
		{
			if(event.type == sf::Event::Closed)
				m_window.close();
			m_gsm.handleEvent(event);
		}
		m_gsm.update();

		m_window.clear();
		m_gsm.render(m_window);
		m_window.display();
	}
	return 0;
}

App::~App()
{
	m_gsm.dispose();
}


void App::setTitle(std::string title)
{
	m_title = title;
	m_window.setTitle(m_title);
}

std::string App::getTitle()
{
	return m_title;
}

StateHandler& App::getStateManager()
{
	return m_gsm;
}

sf::RenderWindow& App::getWindow()
{
	return m_window;
}
