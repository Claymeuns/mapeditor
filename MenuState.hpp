/*
*
*	MenuState.hpp: The Main Menu's State, will alow
*	the user to load an existing map to edit it or create a new map
*
*/
#ifndef MENU_STATE_HPP
#define MENU_STATE_HPP

#include "State.hpp"
#include "App.hpp"
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "GUI/TextButton.hpp"
#include "GUI/Label.hpp"
#include "GUI/UI.hpp"
#include "GUI/Window.hpp"
#include <SFML\Graphics.hpp>
#include <memory>

///
/// @class MenuState MenuState.hpp
/// @author Cl�ment Sibille
/// 
class MenuState : public State
{
private:
	TextureManager m_textures;
	FontManager m_fonts;
	GUI::Window* m_window;
	GUI::UI m_ui;
	GUI::TextButton m_button;

	void updateWindow(GUI::Window *&window);
	void renderWindow(GUI::Window *&window, sf::RenderWindow& sfmlWindow);
	void handleWindowEvent(GUI::Window *&window, sf::Event& event);
public:
	MenuState(App* app);
	~MenuState();
	virtual void init();
	virtual void handleEvent(sf::Event& event);
	virtual void update();
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void initRessources();
	void initUi();
};

#endif