#include "TextureManager.hpp"

TextureManager::TextureManager()
{

}

void TextureManager::loadTexture(const std::string& key, const std::string& textureFile)
{
	sf::Texture texture;
	texture.loadFromFile(textureFile);

	m_map[key] = texture;
}

sf::Texture& TextureManager::getTexture(const std::string& key)
{
	return m_map.at(key);
}