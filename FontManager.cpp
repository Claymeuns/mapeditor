#include "FontManager.hpp"

FontManager::FontManager()
{

}

void FontManager::loadFont(const std::string& key, const std::string& FontFile)
{
	sf::Font Font;
	Font.loadFromFile(FontFile);

	m_map[key] = Font;
}

sf::Font& FontManager::getFont(const std::string& key)
{
	return m_map.at(key);
}