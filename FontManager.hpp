#ifndef FONT_MANAGER_HPP
#define FONT_MANAGER_HPP

#include <map>
#include <SFML/Graphics.hpp>

///
/// @class FontManager FontManager.hpp
/// @author Cl�ment Sibille
/// @brief To manage Fonts
/// 
class FontManager
{
private:
	/// < the fonts's map
	std::map<sf::String, sf::Font> m_map;
public:
	FontManager();

	/// @brief Used to load fonts
	/// @param key The key we want to associate to the font
	/// @param fontFile The path to the font we want to load
	void loadFont(const std::string& key, const std::string& fontFile);

	/// @brief returns a font we loaded before
	/// @return reference to a loaded font
	sf::Font& getFont(const std::string& key);
};

#endif