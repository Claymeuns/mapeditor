#include "StateHandler.hpp"
#include "State.hpp"
#include "MenuState.hpp"
#include "App.hpp"


StateHandler::StateHandler(App* app)
{
	m_app = app;
	pushState(new MenuState(m_app));
}

void StateHandler::pushState(State* state)
{
	m_states.push(state);
}

void StateHandler::popState()
{
	delete m_states.top();
	m_states.pop();
}

void StateHandler::changeState(State* state)
{
	if(!m_states.empty())
		popState();
	pushState(state);
}

State* StateHandler::peekState()
{
	if(m_states.empty())
		return nullptr;
	return m_states.top();
}


// Called by the App, Calling the State functions
void StateHandler::init()
{
	peekState()->init();
}

void StateHandler::handleEvent(sf::Event& event)
{
	peekState()->handleEvent(event);
}

void StateHandler::update()
{
	peekState()->update();
}

void StateHandler::render(sf::RenderWindow& window)
{
	peekState()->render(window);
}

void StateHandler::dispose()
{
	peekState()->dispose();
}

// -----------------------------------------------