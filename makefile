CC = g++
CFLAGS = -Wall -g -std=c++14
LDFLAGS = -lm
LIBS=-lsfml-graphics -lsfml-window -lsfml-system -lBox2D
EXEC=MapEditor

all: $(EXEC)

MapEditor: EditState.o FontManager.o TextureManager.o GUI.o MenuState.o StateHandler.o App.o main.o
	$(CC) -o $@ $^ $(LDFLAGS) $(LIBS)

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)