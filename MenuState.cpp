#include "MenuState.hpp"
#include "EditState.hpp"
#include <iostream>

MenuState::MenuState(App *app)
{
	setApp(app);
	m_window = nullptr;
}

MenuState::~MenuState()
{
	dispose();
}

void MenuState::init()
{
	initRessources();
	initUi();
}

void MenuState::handleEvent(sf::Event& event)
{
	if (event.type == sf::Event::KeyPressed)
	{
		m_window = new GUI::Window;
		m_window->setSize(sf::Vector2f(300, 400));
		m_window->setRelativePosition(sf::Vector2i(GUI::CENTER, GUI::CENTER), &getApp()->getWindow());
		m_window->setTitle("Test", m_fonts.getFont("font"));
	}
	if (event.type == sf::Event::Resized)
	{
		getApp()->getWindow().setView(sf::View(sf::FloatRect(0, 0, event.size.width, event.size.height)));
	}
	if (m_window != nullptr)
		m_window->onEvent(event);

	m_ui.onEvent(event);
	
	if (m_button.isClicked())
		getApp()->getStateManager().changeState(new EditState(getApp()));
}

void MenuState::update()
{
	m_ui.onUpdate();
	updateWindow(m_window);
}

void MenuState::render(sf::RenderWindow& window)
{
	window.clear(sf::Color(240,240,240,255));
	m_ui.onRender(window);
	if (m_window != nullptr)
		m_window->onRender(window);
}

void MenuState::dispose()
{
	m_ui.onDispose();
}

void MenuState::initRessources()
{
	m_textures.loadTexture("button", "data/ui/button.png");
	m_fonts.loadFont("font", "data/ui/font.ttf");
}


void MenuState::initUi()
{
	
	m_button.setFont(m_fonts.getFont("font"));
	m_button.setText("Bouton");

	m_ui.addElement(&m_button);
}

void MenuState::updateWindow(GUI::Window *&window)
{
	if (window != nullptr && window->isClosed())
	{
		delete window;
		window = nullptr;
	}

	if (window != nullptr)
		window->onUpdate();
}
void MenuState::renderWindow(GUI::Window *&window, sf::RenderWindow& sfmlWindow)
{

}
void MenuState::handleWindowEvent(GUI::Window *&window, sf::Event& event)
{

}